# RecyclerView控件总结<br>
### 简介：<br>
RecyclerView是support-v7包中的新组件，是一个强大的滑动组件，与经典的ListView相比，同样拥有item回收复用的功能，这一点从它的名字Recyclerview即回收view也可以看出。

### 基本使用：
1. 引用

```
//在app的build.grade文件的dependencies中添加
implementation 'com.android.support:recyclerview-v7:28.0.0'
    //添加完后记得同步
```

2. xml布局文件: RV 与 Item

```
//在父布局xml中加入
<android.support.v7.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:dividerHeight="10dp"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />

//item.xml与listview的一样，但是需要注意高度的设置
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="120dp"
    android:orientation="horizontal">

    <ImageView
        android:id="@+id/rvicon"
        android:layout_width="100dp"
        android:layout_height="100dp"
        android:src="@mipmap/ic_launcher"
        android:scaleType="fitCenter"
        android:layout_margin="5dp"
        />
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="100dp"
        android:orientation="vertical"
        android:layout_marginTop="5dp"
        android:layout_marginLeft="5dp"
        >
        <TextView
            android:id="@+id/rvtext1"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="名字"
            android:textSize="30sp"
            android:layout_marginTop="10dp"/>
        <TextView
            android:id="@+id/rvtext2"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="问候内容"
            android:textSize="24dp"
            android:layout_marginTop="10dp"/>
    </LinearLayout>
</LinearLayout>
```

3. 创建适配器<br>
本质与listview一样，但实现时必须遵循 **ViewHolder** 设计模式。<br>

步骤如下：
* 创建Adapter（继承RecyclerView.Adapter<ViewHolder>的Adapter类)
* 创建ViewHolder(在Adapter中创建一个继承RecyclerView.ViewHolder的静态内部类)<br>
PS: 和ListView的Holder的实现方法基本一致。
* 在Adapter中实现3个方法：
    * `public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType)`
    与listview的getView前半部分思路相似，但把内容封装在了holder中。<br>
        * 需要注意的是在onCreateViewHolder()中，映射Layout必须为<br>`View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_1, parent, false);`, 其中root不能为null，原因参考项目一的实验报告。<br>
* `public void onBindViewHolder(final myViewHolder holder, int position)`<br>
用于适配渲染数据到View中<br>
* public int getItemCount()<br>
与自定义Adapter的getCount方法类似

可以看出，RecyclerView将ListView中getView()的功能拆分成了onCreateViewHolder()和onBindViewHolder(), 整体的思路也与listview的自定义Adapter相近<br>


```
//RVAdapter.java
//1. 创建Adapter
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.RViewHolder> {
    //2. 创建VH
    public static class RViewHolder extends RecyclerView.ViewHolder{
        public final ImageView imageView;
        public final TextView text1;
        public final TextView text2;
        //特点一：避免了重复findById, 减少开销

        public RViewHolder(View v){
            super(v);
            imageView = (ImageView)v.findViewById(R.id.rvicon);
            text1 = (TextView)v.findViewById(R.id.rvtext1);
            text2 = (TextView)v.findViewById(R.id.rvtext2);
        }
    }

    //3. datas数据集合以及构造方式
    private List<Brother> datas;//同样需要一个list
    public RVAdapter(List<Brother> datas){
        this.datas = datas;
    }

    //4.实现三个方法

    @Override
    public  RViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
         View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemforrecyclerview, parent, false);
         return new RViewHolder(v);
    }//加载item根布局，将以VH的形式传给onBindViewHolder

    @Override
    public  void  onBindViewHolder(RViewHolder holder, int position){
        holder.imageView.setImageResource(datas.get(position).getImageview());
        holder.text1.setText(datas.get(position).getName());
        holder.text2.setText(datas.get(position).getWords());
    }//对VH中的对象进行设置

    @Override
    public int getItemCount() {
        if (datas == null){
            return 0;
        }
        return datas.size();
    }//依旧是必须存在的函数

}


//MainActivity.java
List<Brother> list = new ArrayList<>();
list.add(new Brother("老大","我是老大，你好",R.drawable.icon2));
list.add(new Brother("老二","我是老二，你好",R.drawable.icon2));
list.add(new Brother("老三","我是老三，你好",R.drawable.icon2));

LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
RVAdapter rvAdapter = new RVAdapter(list);
RecyclerView mrv = (RecyclerView) findViewById(R.id.recyclerview);
mrv.setLayoutManager(manager);
mrv.setAdapter(rvAdapter);
```

4. 四大设置：<br>
Layout Manager(必选)：Item的布局。<br>
Adapter(必选)：为Item提供数据。<br>
Item Decoration(可选，默认为空)：Item之间的Divider。<br>
Item Animator(可选，默认为DefaultItemAnimator)r：添加、删除Item动画。<br>

* LayoutManager负责RecyclerView的布局，其中包含了Item View的获取与回收。RecyclerView提供了三种布局管理器：
LinerLayoutManager 以垂直或者水平列表方式展示Item<br>
GridLayoutManager 以网格方式展示Item<br>
StaggeredGridLayoutManager 以瀑布流方式展示Item<br>
如果你想用 RecyclerView 来实现自己自定义效果，则应该 **去继承实现自己的 LayoutManager，并重写相应的方法** ，而不应该想着去改写 RecyclerView。<br>
PS: 自定义LayoutManager学习资料[https://github.com/hehonghui/android-tech-frontier/blob/master/issue-9/%E5%88%9B%E5%BB%BA-RecyclerView-LayoutManager-Part-1.md]


```
//LinearLayoutManager的构造方式
new LinearLayoutManager(Context context)//参数为上下文环境，实现的是默认的垂直布局
new LinearLayoutManager( Context context, int orientation, boolean reverseLayout)//第一个参数为上下文环境，第二个参数为布局显示方式，第三个参数为布尔值是否反转
```


* 添加点击事件属性：

```
//Adapter中：
// ① 定义点击回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
        void onItemLongClick(View view, int position);
    }
    
    // ② 定义一个设置点击监听器的方法
    public void setOnItemClickListener(MyAdapter.OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
    
    //在onBindViewHolder中
//③ 对RecyclerView的每一个itemView设置点击事件
    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            if(onItemClickListener != null) {
                int pos = holder.getLayoutPosition();
                onItemClickListener.onItemClick(holder.itemView, pos);
            }
        }
    });

    holder.itemView.setOnLongClickListener(new View.OnLongClickListener()           @Override
        public boolean onLongClick(View v) {
            if(onItemClickListener != null) {
                int pos = holder.getLayoutPosition();
                onItemClickListener.onItemLongClick(holder.itemView, pos);
            }
            //表示此事件已经消费，不会触发单击事件
            return true;
        }
    });

```
* 万能适配器：<br>
    即定义一个抽象类，用的时候把没写的方法（抽象方法convert）再写上。
    * 问题一： `holder.itemView.setOnLongClickListener`等怎么处理，接口不能在类的外部用<br>
    解决方法： 不放到convert中！！！ && 善于用(List<Food>)之类的转换类型
    * 问题二: 使用了该方法后，结果部分语句出错：`mAdapter.getItem(position).getName();`，`mAdapter.getItem(position)`。<br>
    因为此时返回的都是T，所以我们在前面加上(Food)类型转换即可。
    

```
//MainActivity.java中加入
mAdapter = new MyRecyclerViewAdapter<Food>(Datas){
    @Override
    public void convert(final MyViewHolder holder, Food data, int position){
        TextView text = holder.getView(R.id.food_text);
        text.setText(data.getName());

        TextView icon = holder.getView(R.id.food_icon);
        icon.setText(data.getIcon_name());
    }
};

//RVAdapter.java
public abstract class MyRecyclerViewAdapter<T> extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder> {

    //创建VH
    static class MyViewHolder extends RecyclerView.ViewHolder {
        private SparseArray<View> views;
        private View view;
        //构造器
        public MyViewHolder(View v){
            super(v);
            view = v;
            views = new SparseArray<>();
        }

        public static MyViewHolder get(Context _context, ViewGroup _viewGroup, int _layoutId) {
            View _view = LayoutInflater.from(_context).inflate(_layoutId, _viewGroup, false);
            MyViewHolder holder = new MyViewHolder(_view);
            return holder;
        }

        public <T extends View> T getView(int _viewId) {
            View _view = views.get(_viewId);
            if (_view == null) {
                // 创建view
                _view = view.findViewById(_viewId);
                // 将view存入views
                views.put(_viewId, _view);
            }
            return (T)_view;
        }
    }

    //大部分内容都不变 -> onBindViewHolder中加入convert抽象方法！！！
    //注意这里是T， 不是Food, 且需要类传入
    private List<T> mDatas;
    private MyRecyclerViewAdapter.OnItemClickListener onItemClickListener;



    public MyRecyclerViewAdapter(List<T> mDatas){
        //初始化数据集合
        this.mDatas = mDatas;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        //加载布局
        MyViewHolder holder = MyViewHolder.get(parent.getContext(), parent, R.layout.activity_recyclerview);
        return holder;
    }


    //定义抽象方法convert
    public abstract void convert(MyViewHolder holder, T data, int position);
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        convert(holder, mDatas.get(position), position);
//        // 绑定视图
//        TextView text = holder.getView(R.id.food_text);
//        text.setText(mDatas.get(position).getName());
//
//        TextView icon = holder.getView(R.id.food_icon);
//        icon.setText(mDatas.get(position).getIcon_name());
//
        //OnClickListener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if(onItemClickListener != null) {
                    int pos = holder.getLayoutPosition();
                    onItemClickListener.onItemClick(holder.itemView, pos);
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(onItemClickListener != null) {
                    int pos = holder.getLayoutPosition();
                    onItemClickListener.onItemLongClick(holder.itemView, pos);
                }
                return true;
            }
        });

    }

    @Override
    public int getItemCount()
    {
        if(mDatas == null){
            return 0;
        }
        return mDatas.size();
    }

    public void addNewItem(T f) {
        if(mDatas == null) {
            mDatas = new ArrayList<>();
        }
        mDatas.add(0, f);
        notifyItemInserted(0);
    }

    public void deleteItem(int pos) {
        if(mDatas == null || mDatas.isEmpty()) {
            return;
        }
        mDatas.remove(pos);
        notifyItemRemoved(pos);
    }

    public T getItem(int pos){
        if(mDatas == null || mDatas.isEmpty())
            return null;
        return mDatas.get(pos);
    }

    public boolean updateData(String name, Boolean like) {
        for(Food i : (List<Food>)mDatas){
            if(i.getName().equals(name)){
                if(i.getLike().equals(like))
                    return false;
                i.setLike(like);
                return true;
            }
        }
        return false;
    }

    //RecyclerView没有OnItemClickListener方法，需要在Adapter中实现。

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
        void onItemLongClick(View view, int position);
    }

    public  void setOnItemClickListener(OnItemClickListener _onItemClickListener) {
        this.onItemClickListener = _onItemClickListener;
    }

}
```
注意： 只重写convert，并且只把需要重写的部分放在convert中！！！

